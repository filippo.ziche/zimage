/**
 * MIT License
 *
 * Copyright (c) 2020 Filippo Ziche
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>

#define FREEIMAGE_LIB
#include "../vendor/FreeImage/FreeImage.h"
#include "../vendor/dirent/dirent.h"

#define MAX(a, b) ((a > b) ? a : b)
#define TREEVIEW 1

struct ARGS {
    bool recursive;
    bool ask_to_convert;
} args;

// Converte un'immagine in .webp in un'immagine .png
// richiede il nome del file da convertire e il suo nome finale
bool ConvertImageToPNG(const char* input, const char* output) {
    
    FREE_IMAGE_FORMAT format = FreeImage_GetFileType(input, 0);
    if (format != FIF_WEBP) {
        fprintf(stderr, "Immagine non supportata: '%s'\n", input);
        return false;
    }

    FIBITMAP* bitmap = FreeImage_Load(format, input);
    if (bitmap == NULL) {
        fprintf(stderr, "Errore caricamento immagine: '%s'\n", input);
        return false;
    }

    if (!FreeImage_Save(FIF_PNG, bitmap, output, PNG_DEFAULT)) {
        fprintf(stderr, "Errore salvataggio immagine: '%s' ('%s')\n", output, input);
        return false;
    }

    FreeImage_Unload(bitmap);
    return true;
}

void inline PrintIndent(const char* val, int count) {
    for (int i = 0; i < count; i++)
        printf("%s", val);
}

inline bool AskUserToConvert(const char* filename) {
    char res;
    while (true) {
        printf("Convertire '%s' a .png? (y/n) ", filename);
        fflush(stdout);

        scanf("%c%*c", &res);
        if (res == 'y' || res == 'Y')
            return true;
        else if (res == 'n' || res == 'N')
            return false;
    };
}

// Converte tutte le immagini nella directory e se richiesto nelle sotto-directory
void ConvertDirectory(const char* directory, int depth = 0) {
    
    DIR* dir;
    dir = opendir(directory);
    if (dir != NULL) {
        
        char path[PATH_MAX+1];
        char outpath[PATH_MAX+1];

        struct dirent* ent;
        while((ent = readdir(dir)) != NULL) {
            if (strcmp(".", ent->d_name) == 0 || strcmp("..", ent->d_name) == 0)
                continue;

            switch (ent->d_type) {
            
            // File, converte solo .webp
            // I nomi sono 'originale'_.png se gli originali non vengono eliminati
            case DT_REG: {
                const char* dot = strrchr(ent->d_name, '.');
                if (dot != NULL && strcmp(".webp", dot) == 0) {

                    bool convert = true;
                    if (args.ask_to_convert)
                        convert = AskUserToConvert(ent->d_name);

                    if (convert) {

                        char outname[PATH_MAX + 1];
                        int len = (size_t)dot - (size_t)ent->d_name;
                        strncpy(outname, ent->d_name, len); 

                        snprintf(path, PATH_MAX, "%s/%s", directory, ent->d_name);
                        snprintf(outpath, PATH_MAX, "%s/%s.png", directory, outname);
                        ConvertImageToPNG(path, outpath);

#if TREEVIEW
                        printf("IMG | ");
                        PrintIndent("  ", depth);
                        printf("%s -> %s.png\n", ent->d_name, outname);
#endif
                    }
                }
            } break;

            // Directory, avanza solo se la flag recursive è 
            // settata a true
            case DT_DIR: {
                if (args.recursive) {
#if TREEVIEW
                    printf("DIR | ");
                    PrintIndent("  ", depth);
                    printf("%s: \n", ent->d_name);
#endif
                    snprintf(path, PATH_MAX, "%s/%s", directory, ent->d_name);
                    ConvertDirectory(path, depth + 1);
                }

            } break;

            default:
                break;
            }
        }
        closedir(dir);

    } else {
        printf("Cartella non trovata\n");
    }
}

static const char* USAGE = 
    "\n"
    "Utilizzo:\n"
    "   zimage.exe <cartella> [-ry]\n\n"
    "Flags:\n"
    "   -r --recursive      Converte anche le immagini nelle sotto-cartelle\n"
    "   -y --yes            Non chiede conferma prima di convertire le immagini\n\n"
    "Esempio:\n"
    "   zimage.exe . -y     Converte tutte le immagini di questa cartella senza\n"
    "                       chiedere la conferma prima di farlo\n\n";

int main(int argc, char* argv[]) {
    
    // Impostazioni standard
    args.recursive = false;
    args.ask_to_convert = true;

    // zimage.exe NON CONCESSO
    if (argc < 2) {
        printf(USAGE);
        return EXIT_FAILURE;
    }
    // zimage.exe <dir> [flags]
    else if (argc >= 2) {
        int flags_count = 0;
        for(int i = 1; i < argc; i++) {
            if(argv[i][0] == '-') {
                flags_count += 1;

                if (strcmp("-r", argv[i]) == 0 || strcmp("--recursive", argv[i]) == 0) {
                    args.recursive = true;
                }
                else if (strcmp("-y", argv[i]) == 0 || strcmp("--yes", argv[i]) == 0) {
                    args.ask_to_convert = false;
                }
                else {
                    printf("Flag non riconosciuta: %s\n", argv[i]);
                    printf(USAGE);
                    return EXIT_FAILURE;
                }
                
            }
        }

        if (flags_count == argc - 1) {
            printf("Cartella non specificata\n");
            printf(USAGE);
            return EXIT_FAILURE;
        }
    }
    // zimage.exe <dir>
    else {
        // ... 
    }

    ConvertDirectory(argv[1], 0);
    return EXIT_SUCCESS;
}
